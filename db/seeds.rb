Room.destroy_all
Category.destroy_all
RoomCategory.destroy_all
Device.destroy_all

['Living Room', 'Bedroom', 'Kitchen', 'Hall', 'Office'].each do |room|
	Room.create(name: room)
end

puts 'created rooms'

['Alarm', 'Audio', 'Lighting', 'Heating'].each do |cat|
	Category.create(name: cat)
end

puts 'created category'

Category.where(name: 'Lighting').each do |cat|
	Room.find_each do |room|
		room.categories << cat
	end
end

p 'created room_categories'

RoomCategory.find_each do |rc|
	Device.create(name:'żyrandol', on: false, type: 'switch', room_category: rc)
end






=begin
a=Room.create(name:'Living Room')
Room.create(name:'Bedroom')
Room.create(name:'Kitchen')
Room.create(name:'Hall')
Room.create(name:'Office')

b=Category.create(name:'Alarm')
Category.create(name:'Audio')
Category.create(name:'Lighting')
Category.create(name:'Heating')

c=RoomCategory.create(room:a,category:b)

Devices.create(name:'Radio',on:true,type:'Music',room_category:c)
=end