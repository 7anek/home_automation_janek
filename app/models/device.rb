class Device < ActiveRecord::Base
  belongs_to :room_category
  self.inheritance_column = :dev_type
end
