Rails.application.routes.draw do
  get 'admin/index', as: :admin

  get '/room/:id', to: 'main#room', as: :main_room
  get '/category/:id', to: 'main#category', as: :main_category

  devise_for :users
  resources :devices

  resources :room_categories

  resources :categories

  resources :rooms

  #get 'main/index'
  root 'main#index'
end
